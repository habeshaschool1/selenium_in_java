package testNG;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class A_ClassAndMethodAnnotations {
	WebDriver driver;
  
	@BeforeClass
	public void open_the_website() {
		 System.setProperty("webdriver.chrome.driver", utility.PathList.chromeDriver);
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);	// wait will be applied for any element not available for the first try

			driver.get("http://localhost:8888/test/testpage.php"); // opening a website
	     
		
	}
	
  @BeforeMethod
  public void xyz() {
	  // clear some data before test 
	  // make sure there is not data in the database which disturbs your test 
	  driver.findElement(By.xpath("//input[@name='name']")).clear();;
	  driver.findElement(By.xpath("//input[@name='name']")).sendKeys("Firstname Lastname");
		
	 
  }
  
  @AfterMethod
  public void after_method() {
	  // if i have some data in the database ==> clear that data 
  }
  
  @AfterClass
	public void close_driver() {
		   driver.close();
		   
	}



// ######################## TEST ####################################
  
  	@Test(dataProvider ="test_data")
    public void test_valid_emails(String email, String expectedErrorTxt) throws InterruptedException {
	  // write code here 
	// Testing check box
  			
	        // type email
	        driver.findElement(By.xpath("//input[@id='eml']")).sendKeys(email);
	        Thread.sleep(4000);
	        driver.findElement(By.xpath("//input[@name='submit']")).click();
	        Thread.sleep(2000);
	       String error = driver.findElement(By.xpath("//span[@class='error'][2]")).getText();
	       Assert.assertTrue(error.equals(expectedErrorTxt));
	     
  }
  	
  	
  	@DataProvider
  	 public Object[][] test_data(){
  		String valid_message = "*";
  		String invalid_message = "* Invalid email format";
  	 return new Object[][] {
  		 {"a@gmail.com", valid_message}, 
  		 {"a@gmail.com.org", valid_message}, 
  		 {"a-s@gmail.com.org", valid_message}, 
  		 {"a.s@gmail.com.org", valid_message}, 
  		 {"a.s@gmailcom.org", valid_message}, 
  		 {"1@gmail.com.org", valid_message}, 
  		 {"agmailcom", invalid_message},
  		 {"a s@gmail.com.org", invalid_message}, 
  		 {"a@@gmail.com", invalid_message},
  		 {"a@gmailcom", invalid_message},
  		 {"a@ gmail.com", invalid_message},
  		 {"agmail.com", invalid_message},
  		 {"a gmail.com", invalid_message},
  		 {"a@gmail", invalid_message},
  		 {"@gmail.com", invalid_message},
  		 {" @gmail.com", invalid_message},
  		 {"a@gmail,com", invalid_message},
  		 {"a@gmail.", invalid_message},
  		 {"a@gmail. ", invalid_message},
  		 {"a@.com", invalid_message},
  		 {".a@gmail.com", invalid_message},
  		 {"a@gmail@gmail.com", invalid_message},
  		 {"a@gmail@com", invalid_message},
  		 {" ", invalid_message},
 		 {"", invalid_message}
  		 };
  	 }
  	
  	
  
}
