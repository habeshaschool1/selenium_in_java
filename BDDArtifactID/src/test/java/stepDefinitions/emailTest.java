package stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import BDDGroupIID.BDDArtifactID.TestRunnerTestNG;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class emailTest {

	WebDriver driver = TestRunnerTestNG.getDriver();

@Given("^I am on home page \"([^\"]*)\"$")
public void i_am_on_home_page(String url) throws Throwable {
	driver.get(url);
}

@When("^I type email as \"([^\"]*)\"$")
public void i_type_email_as(String email) throws Throwable {
    driver.findElement(By.xpath("//input[@id='eml']")).sendKeys(email);
    Thread.sleep(4000);
}

@When("^I press submit button$")
public void i_press_submit_button() throws Throwable {
    driver.findElement(By.xpath("//input[@name='submit']")).click();
    Thread.sleep(2000);
}

@Then("^I get \"([^\"]*)\" message$")
public void i_get_message(String message) throws Throwable {
    String error = driver.findElement(By.xpath("//span[@class='error'][2]")).getText();
    Assert.assertTrue(error.equals(message));
}


}
