#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Test valid and invalid scenarios for email


  Scenario Outline: Title of your scenario outline
    Given I am on home page "http://localhost:8888/test/testpage.php"
    When I type email as "<email>"
    And I press submit button
    Then I get "<message>" message

    Examples: 
      | email  | message | 
      | a@gmail.com |     * | 
      | a@@gmail.com |     * Invalid email format | 
